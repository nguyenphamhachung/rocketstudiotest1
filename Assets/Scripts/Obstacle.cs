using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public List<Material> materials = new List<Material>();
    public BodyColor obstacleColor;
    public enum BodyColor
    {
        Red,
        Green,
        Blue,
    }
    private void Start()
    {
        switch(obstacleColor)
        {
            case BodyColor.Red:
                GetComponent<MeshRenderer>().material = materials[0]; 
                break;
            case BodyColor.Green:
                GetComponent<MeshRenderer>().material = materials[1];
                break;
            case BodyColor.Blue:
                GetComponent<MeshRenderer>().material = materials[2];
                break;
        }
    }


}
