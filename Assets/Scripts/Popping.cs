using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Popping : MonoBehaviour
{
    public float bigRatio = 1.2f;
    public float smallRatio = 0.8f;
    float originalScale;
    void Start()
    {
        originalScale = transform.localScale.x;
        //InvokeRepeating("PoppingObject", 0f, 1f);
        PoppingObject();
    }

    void PoppingObject()
    {
        transform.DOScale(bigRatio * originalScale, .5f)
            .OnComplete(() =>
            {
                transform.DOScale(smallRatio * originalScale, .5f)
                .OnComplete(() =>
                {
                    PoppingObject();
                });
            });
    }
}
