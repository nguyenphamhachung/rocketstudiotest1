using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class PrecheckOverlapped : MonoBehaviour
{
    public bool isOverlapped = false;
    
    private void OnTriggerEnter(Collider other)
    {
        isOverlapped = true;
    }
    private void OnTriggerExit(Collider other)
    {
        isOverlapped = false;
    }
}
