using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floating : MonoBehaviour
{
    
    void Start()
    {
        ObjFloating();
    }

    void ObjFloating()
    {
        transform.DOMoveY(transform.position.y + 1f, 1f).SetEase(Ease.Linear)
            .OnComplete(() =>
            {
                transform.DOMoveY(transform.position.y - 1f, 1f).SetEase(Ease.Linear)
                .OnComplete(() =>
                {
                    ObjFloating();
                });
            });
    }
}
