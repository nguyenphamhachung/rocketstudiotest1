using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Feet : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag!="Player")
        {
            PlayerController.instance.isGrounded = true;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag != "Player")
        {
            PlayerController.instance.isGrounded = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag != "Player")
        {
            PlayerController.instance.isGrounded = false;
        }
    }
}
