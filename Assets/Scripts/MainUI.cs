using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainUI : MonoBehaviour
{
    public static MainUI instance;
    public GameObject blackPanel;
    public GameObject blackOut;
    public GameObject gameOverPanel;
    public GameObject winPanel;
    public Text scoreText;
    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        BlackIn();
    }  
    public void TurnOff(GameObject gameObject)
    {
        gameObject.transform.DOScale(0f, 1f)
            .OnComplete(() =>
            {
                gameObject.SetActive(false);
                blackPanel.SetActive(false);
            });
    }
    public void TurnOn(GameObject gameObject)
    {
        gameObject.SetActive(true);
        blackPanel.SetActive(true);
        gameObject.transform.localScale = Vector3.zero;
        gameObject.transform.DOScale(1f, 1f);
    }
    public void UpdateScore(int score)
    {
        scoreText.text = "Score: " + score.ToString();
    }
    public void Replay()
    {
        blackOut.SetActive(true);
        blackOut.GetComponent<Image>().color = new Color(0f, 0f, 0f, 0f);
        blackOut.GetComponent<Image>().DOColor(new Color(0f, 0f, 0f, 1f), 1f)
            .OnComplete(() =>
            {
                SceneManager.LoadScene("MainGame");
            });
        
    }
    public void Home()
    {
        blackOut.SetActive(true);
        blackOut.GetComponent<Image>().color = new Color(0f, 0f, 0f, 0f);
        blackOut.GetComponent<Image>().DOColor(new Color(0f, 0f, 0f, 1f), 1f)
            .OnComplete(() =>
            {
                SceneManager.LoadScene("Menu");
            });
    }
    public void BlackOut()
    {
        blackOut.SetActive(true);
        blackOut.GetComponent<Image>().color = new Color(0f, 0f, 0f, 0f);
        blackOut.GetComponent<Image>().DOColor(new Color(0f, 0f, 0f, 1f), 1f);
    }
    public void BlackIn()
    {
        blackOut.GetComponent<Image>().color = new Color(0f, 0f, 0f, 1f);
        blackOut.GetComponent<Image>().DOColor(new Color(0f, 0f, 0f, 0f), 1f)
            .OnComplete(() =>
            {
                blackOut.SetActive(false);
            });
    }
}
