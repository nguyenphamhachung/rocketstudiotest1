using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Obstacle; 

public class PlayerStats : MonoBehaviour
{
    public GameObject smokeEffect;
    public Obstacle.BodyColor bodyColor;
    public Obstacle.BodyColor obColor;
    public List<Material> materials = new List<Material>();
    private void Start()
    {
        RandomBodyColor();
    }
    public void UpdateBodyColor()
    {
        switch (bodyColor)
        {
            case BodyColor.Red:
                GetComponent<MeshRenderer>().material = materials[0];
                transform.GetChild(1).GetComponent<MeshRenderer>().material = materials[0];
                break;
            case BodyColor.Green:
                transform.GetChild(1).GetComponent<MeshRenderer>().material = materials[1];
                GetComponent<MeshRenderer>().material = materials[1];
                break;
            case BodyColor.Blue:
                transform.GetChild(1).GetComponent<MeshRenderer>().material = materials[2];
                GetComponent<MeshRenderer>().material = materials[2];
                break;
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.tag == "Obstacle")
        {
            obColor = other.gameObject.GetComponent<Obstacle>().obstacleColor;
            if (bodyColor == obColor)
            {
                GameObject effect = Instantiate(smokeEffect);
                effect.transform.position = other.transform.position;
                Destroy(other.gameObject);
                RandomBodyColor();
                GameManager.instance.AddScore();
                Invoke("DelaySpawn", 1f);
            }
            else
            {
                GameManager.instance.GameOver();
            }
        }
    }
    void DelaySpawn()
    {
        GameManager.instance.SpawnObstacle(obColor);
    }
    void RandomBodyColor()
    {
        Array enumValues = Enum.GetValues(typeof(Obstacle.BodyColor));
        // Generate a random index
        System.Random random = new System.Random();
        int randomIndex = random.Next(enumValues.Length);

        // Get the random enum value
        Obstacle.BodyColor randomEnumValue = (Obstacle.BodyColor)enumValues.GetValue(randomIndex);
        bodyColor = randomEnumValue;
        UpdateBodyColor();
    }
}
