using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public GameObject tutorial;
    public GameObject blackPanel;
    public GameObject blackout;
    private void Start()
    {
        BlackIn();
        tutorial.SetActive(false);
        blackPanel.SetActive(false);
        
    }
    public void TutorialButton()
    {
        blackPanel.SetActive(true);
        tutorial.SetActive(true);
        tutorial.transform.localScale = Vector3.zero;
        tutorial.transform.DOScale(1f, .5f);
    }
    public void ExitButton()
    {
        tutorial.transform.DOScale(0f, .5f)
            .OnComplete(() =>
            {
                blackPanel.SetActive(false);
                tutorial.SetActive(false);
            });
    }
    public void PlayButton()
    {
        blackout.SetActive(true);
        blackout.GetComponent<Image>().color = new Color(0f, 0f, 0f, 0f);
        blackout.GetComponent<Image>().DOColor(new Color(0f, 0f, 0f, 1f), 1f)
            .OnComplete(() =>
            {
                SceneManager.LoadScene("MainGame");
            });
    }
    void BlackIn()
    {
        blackout.SetActive(true);
        blackout.GetComponent<Image>().color = new Color(0f, 0f, 0f, 1f);
        blackout.GetComponent<Image>().DOColor(new Color(0f, 0f, 0f, 0f), 1f)
            .OnComplete(() =>
            {
                blackout.SetActive(false);
            });
    }
    void BlackOut()
    {
        blackout.SetActive(true);
        blackout.GetComponent<Image>().color = new Color(0f, 0f, 0f, 0f);
        blackout.GetComponent<Image>().DOColor(new Color(0f, 0f, 0f, 1f), 1f)
            .OnComplete(() =>
            {
                blackout.SetActive(false);
            });
    }
}