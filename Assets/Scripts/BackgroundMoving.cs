using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BackgroundMoving : MonoBehaviour
{
    public float time;
    void Start()
    {
        MovingBG();
    }

    void MovingBG()
    {
        transform.DOMove(new Vector3(transform.position.x + 20f, transform.position.y + 20f), time).SetEase(Ease.Linear)
            .OnComplete(() =>
            {
                transform.DOMove(new Vector3(transform.position.x + 20f, transform.position.y + -20f), time).SetEase(Ease.Linear)
                .OnComplete(() =>
                {
                    transform.DOMove(new Vector3(transform.position.x + -20f, transform.position.y + 20f), time).SetEase(Ease.Linear)
                    .OnComplete(() =>
                    {
                        transform.DOMove(new Vector3(transform.position.x + -20f, transform.position.y - 20f), time).SetEase(Ease.Linear)
                        .OnComplete(() =>
                        {
                            MovingBG();
                        });
                    });
                });
            });
    }
}
