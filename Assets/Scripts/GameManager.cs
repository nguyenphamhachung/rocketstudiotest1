using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public GameObject obstacle;
    public GameObject precheckOverlap;
    Transform obHolder;
    bool gameOver = false;
    int score;
    int xMax = 27;
    int xMin = -47;
    int zMax = 33;
    int zMin = -37;
    int y = 8;
    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        obHolder = GameObject.FindGameObjectWithTag("ObHolder").transform;
        SpawnObstacle(Obstacle.BodyColor.Red);
        SpawnObstacle(Obstacle.BodyColor.Blue);
        SpawnObstacle(Obstacle.BodyColor.Green);
    }
    public void AddScore()
    {
        score++;
        MainUI.instance.UpdateScore(score);
        if(score == 10)
        {
            MainUI.instance.TurnOn(MainUI.instance.winPanel);
            PlayerController.instance.gameObject.GetComponent<PlayerController>().enabled = false;
        }
    }
    public int GetSCore()
    {
        return score;
    }
    public void GameOver()
    {
        if (!gameOver)
        {
            gameOver = true;
            MainUI.instance.TurnOn(MainUI.instance.gameOverPanel);
            PlayerController.instance.gameObject.GetComponent<PlayerController>().enabled = false;
        }
    }    
    public void SpawnObstacle(Obstacle.BodyColor bodyColor)
    {
        float xToSpawn = Random.Range(xMin, xMax);
        float zToSpawn = Random.Range(zMin, zMax);
        GameObject precheck = Instantiate(precheckOverlap,obHolder);
        precheck.transform.localPosition = new Vector3(xToSpawn, y, zToSpawn);
        while (precheck.GetComponent<PrecheckOverlapped>().isOverlapped)
        {
            Debug.Log("Overlapped");
            xToSpawn = Random.Range(xMin, xMax);
            zToSpawn = Random.Range(zMin, zMax);
            precheck.transform.localPosition = new Vector3(xToSpawn, y, zToSpawn);
        }
        GameObject newOb = Instantiate(obstacle,obHolder);
        newOb.transform.localPosition = new Vector3(xToSpawn, y, zToSpawn);
        newOb.GetComponent<Obstacle>().obstacleColor = bodyColor;
        Destroy(precheck);
    }
}